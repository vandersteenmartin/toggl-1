package mailer

import (
	"log"
)

// Dummy implementation

type logSender struct{}

var logger Sender = logSender{}

func New() Sender {
	return logger
}

func (s logSender) Send(to, from EmailAddress, email Email) (err error) {
	log.Printf("to: %#+v\n", to)
	log.Printf("from: %#+v\n", from)
	log.Printf("email: %#+v\n", email)
	return nil
}
