package mailer

// Email sending interface

type Sender interface {
	Send(to, from EmailAddress, email Email) error
}

type EmailAddress struct {
	Name    string
	Address string
}

type Email struct {
	Subject string
	Body    string
}
