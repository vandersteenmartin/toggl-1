package togglapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type AccessToken struct {
	token      string
	validUntil int64
}

type TestTakerApiResponse struct {
	Test_takers []TestTaker
	Total       uint
}

type TestTaker struct {
	Id                       uint
	Name                     string
	Email                    string
	Url                      string
	Hire_state               string
	Submitted_in_time        bool
	Is_demo                  bool
	Percent                  uint
	Points                   float32
	Started_at               uint
	Finished_at              uint
	Contact_info             ContactInfo
	Test_duration_in_seconds uint
}

func (t TestTaker) hasFinishedAfter(timestamp uint) bool {
	if t.Finished_at > timestamp {
		return true
	}
	return false
}

type ContactInfo struct {
	Phone         string
	Full_name     string
	Street        string
	City          string
	Zip_code      string
	State         string
	Country       string
	Website       string
	Linkedin      string
	Contact_email string
}

const baseURL = "http://th-hw.herokuapp.com/"
const authPayload = `{
											"email": "miklos@toggl.com",
											"password": "123456"
										}` // This would normally use environment variables or configuration files

var accessToken AccessToken

// GetRecentTestTakers returns the list of test takers who finished after the given timestamp
func GetRecentTestTakers(timestamp uint64) ([]TestTaker, error) {
	var limit uint = 10
	var offset uint = 0
	recentTestTakers := []TestTaker{}

	takers, err := fetchTestTakers(limit, offset)
	if err != nil {
		return recentTestTakers, err
	}

	for len(takers) > 0 { // Here I should just stop whenever I start getting test takers that finished before my timestamp
		recentTestTakers = append(recentTestTakers, filterTestTakers(takers, uint(timestamp))...)

		offset += limit
		takers, err = fetchTestTakers(limit, offset)
		if err != nil {
			return recentTestTakers, err
		}
	}

	return recentTestTakers, nil
}

func authenticate() (AccessToken, error) {
	var err error
	accessToken.token, err = fetchAccessTokenString()
	if err != nil {
		return accessToken, err
	}
	accessToken.validUntil = time.Now().Unix() + int64(14*24*time.Hour/1000000000)

	return accessToken, nil
}

func fetchAccessTokenString() (string, error) {
	url := baseURL + "api/v1/auth/authenticate"

	var payload = []byte(authPayload)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 10 * time.Second}
	response, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	body, _ := ioutil.ReadAll(response.Body)

	var data map[string]interface{}

	if err := json.Unmarshal([]byte(body), &data); err != nil {
		return "", err
	}

	return data["access_token"].(string), nil
}

func fetchTestTakers(limit uint, offset uint) ([]TestTaker, error) {
	token, err := getAccessToken()
	if err != nil {
		return []TestTaker{}, err
	}

	url := baseURL + "api/v1/test-takers"
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Bearer "+token.token)

	q := req.URL.Query()
	q.Add("limit", fmt.Sprintf("%d", limit))
	q.Add("offset", fmt.Sprintf("%d", offset))

	req.URL.RawQuery = q.Encode()

	client := &http.Client{Timeout: 10 * time.Second}
	response, err := client.Do(req)
	if err != nil {
		return []TestTaker{}, err
	}
	defer response.Body.Close()

	body, _ := ioutil.ReadAll(response.Body)

	var data TestTakerApiResponse

	if err := json.Unmarshal([]byte(body), &data); err != nil {
		return []TestTaker{}, err
	}

	return data.Test_takers, nil
}

func filterTestTakers(array []TestTaker, timestamp uint) []TestTaker {
	// Here I should also filter test takers that are present in the DB

	result := []TestTaker{}
	for _, cell := range array {
		if cell.hasFinishedAfter(timestamp) && cell.Is_demo == false && cell.Percent >= 80 {
			result = append(result, cell)
		}
	}
	return result
}

func getAccessToken() (AccessToken, error) {
	if accessToken.validUntil > (time.Now().Unix() - 60) {
		return accessToken, nil
	}
	return authenticate()
}
