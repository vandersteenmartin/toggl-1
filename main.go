package main

import (
	"log"
	"sync"
	"toggl-1/mailer"
	"toggl-1/togglapi"

	"github.com/robfig/cron"
)

var waitGroup sync.WaitGroup
var cronInstance = cron.New()

func checkNewTestTakers() {
	testTakers, err := togglapi.GetRecentTestTakers(1582608550) // Here the timestamp should be the finished_at date of the last recorded testTaker in the DB
	if err != nil {
		log.Println(err)
		return
	}

	log.Printf("Emailing %d test takers", len(testTakers))
	for _, user := range testTakers {
		go emailNewTestTaker(user)
	}
}

func emailNewTestTaker(user togglapi.TestTaker) {
	logger := mailer.New()
	var fullname string
	var email string

	if user.Contact_info.Full_name != "" {
		fullname = user.Contact_info.Full_name
	} else {
		fullname = user.Name
	}

	if user.Contact_info.Contact_email != "" {
		email = user.Contact_info.Contact_email
	} else {
		email = user.Email
	}

	logger.Send(mailer.EmailAddress{Name: fullname, Address: email}, mailer.EmailAddress{Name: "Toggl Hire", Address: "hello@hire.toggl.com"}, mailer.Email{Subject: "Thank you", Body: "Thank you for applying via Toggl Hire."})
	// Write user id and finished_at in the DB
}

func main() {
	cronInstance.Start()
	cronInstance.AddFunc("* */10 * * *", checkNewTestTakers)

	waitGroup.Add(1)
	waitGroup.Wait()
}
